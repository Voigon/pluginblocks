package net.apartium.plus.pluginblocks;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.security.SecureRandom;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtNewMethod;
import javassist.NotFoundException;

public class PluginBlocks extends JavaPlugin {

	@SuppressWarnings("unchecked")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof BlockCommandSender))
			return false;

		if (label.equalsIgnoreCase("load")) {
			StringBuilder sb = new StringBuilder();
			for (String string : args)
				sb.append(string + " ");
			
			ClassPool pool = new ClassPool(true);
			pool.importPackage("org.bukkit");
			pool.importPackage("org.bukkit.command");
			pool.importPackage("org.bukkit.entity");

			SecureRandom random = new SecureRandom();
			CtClass cc = pool.makeClass("pluginblocks."+new BigInteger(130, random).toString(32));
			try {
				cc.addMethod(
						CtNewMethod.make("public static void onExecute(org.bukkit.command.BlockCommandSender sender) {"
								+ sb.toString() + "};", cc));
				cc.writeFile();
				cc.toClass().getMethod("onExecute", BlockCommandSender.class).invoke(null, sender);

			} catch (CannotCompileException e) {
				sender.sendMessage("FAIL: compilation error");
				e.printStackTrace();
			} catch (NotFoundException | IOException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}

		}
		
		return false;
	}
}
